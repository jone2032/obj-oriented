﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /*
         * MS: This is all decent code. Remember though that this isn't OO (which was fine this time)
         * but in the future make sure you are doing object oriented.
         */
        private void button_Click(object sender, RoutedEventArgs e)
        {
            String input = textBox.Text;
            int isbn;
            int[] nums = new int[10];
            Boolean X = false;
            char[] charNums = new char[10];

            while (input.Contains("-"))
            {
                int a = input.IndexOf("-");
                input = input.Remove(a, 1);
            }
            if(input.Contains("x") || input.Contains("X"))
            {
                int b = input.IndexOf("x", StringComparison.CurrentCultureIgnoreCase); //comparison thanks to Marty Dill
                input = input.Remove(b, 1);
                X = true;
            }
            Boolean conversion = Int32.TryParse(input, out isbn);

            if (conversion)
            {
                if((isbn.ToString().Length == 10 && X == false) || (X == true && isbn.ToString().Length == 9))
                {
                    charNums = input.ToCharArray();

                    for (int i = 0; i < isbn.ToString().Length; i++)
                    {
                        nums[i] = Convert.ToInt32(charNums[i].ToString());
                    }
                    if (X == true)
                        {
                            nums[9] = 10;
                        }
                }
                else
                {
                    textBlock.Text = "This ISBN number is invalid. Double check the number you inputed and try again.";
                }
            }
            else
            {
                textBlock.Text = "Invalid ISBN input.";
            }

            int total = 0;
            for (int i = 0; i < 9; i++)
            {
                total += nums[i] * (10 - i);
            }

            total = 11-(total % 11);

            if (total == nums[9] || (total == 10 && X == true))
            {
                textBlock.Text = "This is a valid ISBN-10 number!";
            }
            else
            {
                textBlock.Text = "This is an invalid ISBN-10 number! :(";
            }          
        }
    }
}



