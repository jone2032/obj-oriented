﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    class Program
    {
        static void Main(string[] args)
        {
            string filepath = @"test.csv";//Change to fit where you want your test file KEEP CSV

            MyFile file = new MyFile();

            file.ReadAll(filepath);//writes data from file to console
            file.Menu(filepath);//opens main menu of program
        }
    }
}
