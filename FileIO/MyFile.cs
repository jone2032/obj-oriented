﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    class MyFile
    {
        List<string> _lines = new List<string>();//this list holds all the strings from the file

        public void Menu(string filepath)//main menu for program
        {
            /*
             * MS
             * Be mindful of mixing the UI stuff with the data stuff. This is a Console app so 
             * it's a little harder to keep things separate but definitely for a WPF app 
             * don't try to write to the screen from a data access class
             */
            string ans = "";
            Console.WriteLine("\nWould you like to add or delete an entry, read the file or quit? (a/d/r/q)");
            ans = Console.ReadLine();
            if(ans.Equals("a", StringComparison.InvariantCultureIgnoreCase) || ans.Equals("add", StringComparison.InvariantCultureIgnoreCase))
            {
                AddLine(filepath);
                Menu(filepath);
            }
            else if (ans.Equals("d", StringComparison.InvariantCultureIgnoreCase) || ans.Equals("delete", StringComparison.InvariantCultureIgnoreCase))
            {
                if(_lines.Count > 2)
                {
                    DeleteLine(filepath);
                    Menu(filepath);
                }
                else
                {
                    Console.WriteLine("No entries to delete! Try adding a new one!");
                    Menu(filepath);
                }
                
            }
            else if (ans.Equals("r", StringComparison.InvariantCultureIgnoreCase) || ans.Equals("read", StringComparison.InvariantCultureIgnoreCase))
            {
                ReadAll(filepath);
                Menu(filepath);
            }
            else if (ans.Equals("q", StringComparison.InvariantCultureIgnoreCase) || ans.Equals("quit", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("Goodbye!");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Invalid input. Type \"a\" or\"add\", \"d\" or \"delete\", \"r\" or \"read\",\"q\" or \"quit\" .");
                Menu(filepath);
            }
        }

        public void AddLine(string filepath)//allows user to add a line to the file
        {
            string answer = "";
            Console.WriteLine("\nWould you like to add to the file? (y/n)");
            answer = Console.ReadLine().ToString();
            if (answer.Equals("y", StringComparison.InvariantCultureIgnoreCase) || answer.Equals("yes", StringComparison.InvariantCultureIgnoreCase))
            {
                string input = GetInfo();
                _lines.Add(input);
                File.AppendAllText(filepath, input);
                Console.WriteLine("Entry has been added successfully!");

            }
            else if (answer.Equals("n", StringComparison.InvariantCultureIgnoreCase) || answer.Equals("no", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("No file was added.");
                Menu(filepath);
            }
            else
            {
                Console.WriteLine("Error invalid input. Type \"y\" or \"n\"");
                AddLine(filepath); // how bad is it to make this recursive for a bad input? it should only have to reset one memory space.
                /*
                 * MS 
                 * I think this is an OK way to handle this... basically it just makes it a loop because it will unwind all of the calls
                 * immediately on getting a good entry.
                 */
            }
        }

        private string GetInfo()//Directs user input for file entry creation
        {
            //var regexItem = new Regex("^[a-zA-Z\s-]*$");  /future implementation for last name
            Boolean isWord = false;
            Console.WriteLine("Enter a First Name:");
            string fname = Console.ReadLine();
            isWord = fname.Any(c => !char.IsLetter(c));
            while (fname == "" || isWord)
            {
                Console.WriteLine("Invalid input. Name cannot contain numbers, spaces or special characters.");
                Console.WriteLine("Reenter First Name:");
                fname = Console.ReadLine();
                isWord = fname.Any(c => !char.IsLetter(c));

            };
            Console.WriteLine("Enter a Last Name:");
            string lname = Console.ReadLine();
            isWord = lname.Any(c => !char.IsLetter(c)); //for the time being last names cannot contain hyphen or spaces
            while (lname == "" || isWord)
            {
                Console.WriteLine("Invalid input. Name cannot contain numbers, spaces or special characters.");
                Console.WriteLine("Reenter Last Name:");
                lname = Console.ReadLine();
                isWord = lname.Any(c => !char.IsLetter(c));

            };
            Console.WriteLine("Enter an Email address:");
            string email = Console.ReadLine();
            while ((email.Contains("@") && (email.Contains(".com") || email.Contains(".net") || email.Contains(".gov") || email.Contains(".edu"))) == false)
            {
                Console.WriteLine("Invalid email address. Make sure to include the \"@\" and the domain ex. \".com\".");
                Console.WriteLine("Reenter email address:");
                email = Console.ReadLine();

            };
            string message = string.Format("{0},{1},{2}\n", lname, fname, email);//formats string with commas for csv
            return message;
        }

        public void ReadAll(string filepath)//This reads all lines from file, adds them to the file string list _lines and writes them to console
        {

            if (!File.Exists(filepath))
            {
                using (StreamWriter sw = File.CreateText(filepath))//starts file if one not found
                {
                    sw.WriteLine("User data storage file");//title
                    sw.WriteLine("Last Name, First Name, Email");//column headings
                }
            }
            _lines.Clear();
            string[] lines = File.ReadAllLines(filepath);//puts all lines from file into a string array
            for (int i = 0; i < lines.Length; i++)
            {
                _lines.Add(lines[i]);//adds lines to file list
                if(i == 1)//adds entry # heading to column headigns without creating another column in the csv file
                {
                    Console.WriteLine("#:  " + lines[i]);
                }
                else if (i > 1)
                {
                    Console.WriteLine( (i-1) + ":  " + lines[i]);//adds entry number next to entry (these are used for easily determining which row to delete
                }
                else
                {
                    Console.WriteLine("\n" +lines[i]);
                }                            
            }


        }

        public void DeleteLine(string filepath)//this gets user input to delete a line
        {
            string ans = "";
            Console.WriteLine("Which entry would you like to delete? (under #)");
            ans = Console.ReadLine();
            int entry = -1;
            int lastEntry = _lines.Count - 2;
            Boolean convert = Int32.TryParse(ans, out entry);//makes sure it is a number
            if (convert)
            {
                if (entry > 0 && entry < lastEntry+1)//makes sure number entered is appropriate
                {
                    Console.WriteLine("File selected: " + _lines.ElementAt(entry + 1).ToString());
                    while (convert)
                    {
                        Console.WriteLine("Do you want to delete this file? (y/n)");
                        ans = Console.ReadLine();
                        if (ans.Equals("y", StringComparison.InvariantCultureIgnoreCase) || ans.Equals("yes", StringComparison.InvariantCultureIgnoreCase))
                        {
                            convert = false;
                            _lines.RemoveAt(entry+1);//removes line from list
                            File.WriteAllLines(filepath, _lines.ToArray());//writes editted list over old file
                            Console.WriteLine("File deleted successfully");
                        }
                        else if (ans.Equals("n", StringComparison.InvariantCultureIgnoreCase) || ans.Equals("no", StringComparison.InvariantCultureIgnoreCase))
                        {
                            convert = false;
                            Console.WriteLine("File was not deleted.");
                        }
                        else
                        {
                            Console.WriteLine("Invalid input. Type \"y\" or \"n\"");
                        }
                    }                
                }
                else
                {
                    if(lastEntry == 1)
                    {
                        Console.WriteLine("Invalid entry number. There is only 1 entry.");
                    }
                    else
                    {
                        Console.WriteLine("Invalid entry number. Number must be between 1 and " + lastEntry.ToString() + ".");
                    }
                    
                    DeleteLine(filepath);
                }
            }
            else
            {
                Console.WriteLine("Input invalid. Make sure you entered a number.");
                DeleteLine(filepath);
            }
        }
    }
}
